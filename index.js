const Hapi = require('@hapi/hapi');
const Inert = require('@hapi/inert');
const Vision = require('@hapi/vision');
const HapiSwagger = require('hapi-swagger');
const Pack = require('./package');

const users = {};

const init = async () => {
  // init hapi server
  const server = Hapi.server({
    port: 3000,
    host: 'localhost'
  });

  // init socket server
  const io = require('socket.io')(server.listener);
  // Listening socket server
  io.on('connection', (socket) => {
    console.log('A client connected');
    // catch event new user
    socket.on('new-user', (name, data) => {
      if (name in users) {

      } else {
        socket.username = name;
        users[socket.username] = socket;
        console.log(`Add user ${name} to list`);
        updateUsers();
      }
      console.log(`User ${name} connected with data: ${data}`);

    })
    // catch event send message
    socket.on('message', (name, data) => {
      console.log(`Recived data from ${name}: ${data}`);
      io.sockets.emit('messages',{
        username: name,
        content: data
      })
    })

    // Catch to disconnect socket
    socket.on('disconnect', () => {
      console.log('A client disconnected');
    });

    updateUsers = () => {
      io.sockets.emit('usernames', Object.keys(users));
    }
  });

  // Setup swagget
  const swaggerOptions = {
    info: {
      title: 'Demo API Documentation',
      version: '0.0.1',
    }
  };
  await server.register([
    Inert,
    Vision,
    {
      plugin: HapiSwagger,
      options: swaggerOptions
    }
  ]);

  server.route({
    method: 'GET',
    path: '/',
    options: {
      description: 'Get index page',
      notes: 'Returns Home page',
      tags: ['api'],
      handler: (request, h) => {
        console.log('Call to GET method.')
        return 'Hello World!';
      },
    },

  });

  try {
    await server.start();
    console.log('Server running on %s', server.info.uri);
  } catch (err) {
    console.log(err);
  }


};

process.on('unhandledRejection', (err) => {
  console.log(err);
  process.exit(1);
});

init();